package license

import (
	"bytes"
	"fmt"
	"testing"
)

func TestLicenseSubstitute(t *testing.T) {
	// The info for the license
	holder, year := "Test holder", 2016

	// Create a test license
	var lic License = `The current year is YEAR and the current user is NAME`
	expected := fmt.Sprintf(`The current year is %d and the current user is %s`,
		year, holder)

	if actual := string(lic.Substitute(holder, year)); actual != expected {
		t.Errorf(`Expected the license to be
        "%s"
      but was
        "%s"`, expected, actual)
	} //if
} //TestLicenseSubstitute

func TestLicenseWriteTo(t *testing.T) {
	// Create a buffer
	buffer := bytes.Buffer{}

	// Create a test license
	var lic License = `The current year is YEAR and the current user is NAME`

	// Write the license to the buffer
	count, err := lic.WriteTo(&buffer)

	// Check for an error
	if err != nil {
		t.Fatal("Unexpected error:", err.Error())
	} //if

	// Check the number of bytes written
	if expected, actual := int64(len([]byte(lic))), count; actual != expected {
		t.Errorf("Expected to write %d bytes but wrote %d", expected, actual)
	} //if

	// Check the contents of the buffer
	if expected, actual := string(lic), buffer.String(); actual != expected {
		t.Errorf(`Expected the license to be
        "%s"
      but was
        "%s"`, expected, actual)
	} //if
} //TestLicenseWriteTo

func TestGetLicense(t *testing.T) {
	if _, err := GetLicense("bad"); err == nil {
		t.Error(`Expected error for nonexistant license "bad"`)
	} //if

	checkLic := func(lic License, names ...string) {
		if len(names) == 0 {
			t.Fatal("No names passed to checkLic")
		} //if

		// Loop through the names
		for _, name := range names {
			// Check the current name
			if lic, err := GetLicense(name); err != nil {
				t.Error("Unexpected error:", err.Error())
			} else if expected, actual := lic, lic; actual != expected {
				t.Errorf(`Incorrect license of "%s"`, name)
			} //if
		} //for
	}

	// Test for the BSD 2 Clause license
	checkLic(bsd2, "bsd", "BSD")

	// Test for the BSD 3 Clause license
	checkLic(bsd3, "bsd3", "BSD3")

	// Test for the MIT license
	checkLic(mit, "mit", "MIT")

	// Test for the ISC license
	checkLic(isc, "isc", "ISC")
} //TestGetLicense
