package license

import "io"

// Write a license with the year and name to the Writer. If there is an error
// writing to the Writer it will be returned.
//
// Deprecated: Because Info is deprecated so too is this method. This function
// should be replaced in with WriteLic. Currently, Write calls WriteLic with
// Info.Name and Info.Year supplied as holder and year respectively.
func Write(license string, info *Info, out io.Writer) error {
	// Call the new function passing the name and year from info as the holder
	// and year for WriteLic
	return WriteLic(license, info.Name, info.Year, out)
} //Write

// WriteLic is a convenience function that will find a License, apply the
// infomation, and write it to the supplied io.Writer. If an error occurs it
// will be returned immediately.
func WriteLic(licenseType, holder string, year int, out io.Writer) error {
	// Attempt to get the license
	lic, err := GetLicense(licenseType)

	// Check for an error
	if err != nil {
		return err
	} //if

	// Apply the information to the license and write it tothe writer
	_, err = lic.Substitute(holder, year).WriteTo(out)

	// Return any write errors
	return err
} //WriteLic
