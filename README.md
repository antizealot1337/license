# License
[![GoDoc](https://godoc.org/bitbucket.org/antizealot1337/license?status.svg)](https://godoc.org/bitbucket.org/antizealot1337/license)

A simple utility to write a license file to the current directory. It accepts
the license type, year, and name of the copyright holder. If no year is provided
the current year is assumed. Likewise, if no name is provided the currently
logged-in user's name is used instead.

## Installation

````
go get -u bitbucket.org/antizealot1337/license/cmd/license
````

## Usage
Copied from ````license -help````
````
Usage of license:
  -help
        Show the help/usage information and exit
  -holder string
        The name of the copyright holder (default "<current user name>")
  -list
        List the available licenses instead of writing a license file
  -name string
        The name of the license to write (default "MIT")
  -year int
        The year of the copyright (default <current year>)
````

When specifying the name of the license it should be one of the following:
* MIT https://opensource.org/licenses/MIT
* BSD2 https://opensource.org/licenses/BSD-2-Clause *NOTE: You can also use BSD
(without the "2") to get this license.*
* BSD3 https://opensource.org/licenses/BSD-3-Clause
* ISC https://opensource.org/licenses/ISC

## License

Licensed under the terms of the MIT license. See the LICENSE file for more info.
