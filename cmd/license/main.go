package main

import (
	"flag"
	"fmt"
	"os"
	"os/user"
	"time"

	"bitbucket.org/antizealot1337/license"
)

var (
	lic    = "MIT"
	holder string
	year   int
)

func init() {
	// Determine the default year
	year = time.Now().Year()

	// Determine the default name
	if user, err := user.Current(); err != nil {
		// Panic
		panic(err)
	} else {
		// Set the name
		holder = user.Name
	} //if
} //init

func main() {
	// Create the flag to list the available licenses
	list := flag.Bool("list", false,
		"List the available licenses instead of writing a license file")

	help := flag.Bool("help", false, "Show the help/usage information and exit")

	// Create the flag for the name
	flag.StringVar(&holder, "holder", holder, "The name of the copyright holder")

	// Create the flag for the year
	flag.IntVar(&year, "year", year, "The year of the copyright")

	// Create the flag for the license
	flag.StringVar(&lic, "name", lic, "The name of the license to write")

	// Parse the flags
	flag.Parse()

	// Check the list flag
	if *list {
		fmt.Println("Available licenses:")
		fmt.Println("\tBSD(2)")
		fmt.Println("\tBSD3")
		fmt.Println("\tMIT (default)")
		fmt.Println("\tISC")
		return
	} //if

	// Check the showUsage flag
	if *help {
		flag.Usage()
		os.Exit(0)
	} //if

	// Create the file
	file, err := os.Create("LICENSE")

	// Check if there was an error
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(-1)
	} //if

	// Make sure the file is closed
	defer file.Close()

	// Write the license
	license.WriteLic(lic, holder, year, file)
} //main
